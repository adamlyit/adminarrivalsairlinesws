package com.l00119847.flights.arrivals.controllers;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;

import com.l00119847.flights.arrivals.dao.Airline;
import com.l00119847.flights.arrivals.dao.AirlineResource;
import com.l00119847.flights.arrivals.dao.AirlineResourceAssembler;
import com.l00119847.flights.arrivals.dao.AirlinesResourceAssembler;
import com.l00119847.flights.arrivals.dao.AirlinesResource;
import com.l00119847.flights.arrivals.exceptions.AirlineDoesNotExistException;
import com.l00119847.flights.arrivals.exceptions.AirlinesDoNotExistException;
import com.l00119847.flights.arrivals.exceptions.MissingKeyException;
import com.l00119847.flights.arrivals.service.AirlinesService;

/**
 * This Controller handles arrivals airlines. Uses {@link RestTemplate @RestTemplate}, to
 * execute the HTTP method to the given URI template and returns the response as
 * ResponseEntity. {@link RestTemplate @RestTemplate} is Spring's central class
 * for client-side HTTP access. It simplifies communication with HTTP servers,
 * and enforces RESTful principles. It handles HTTP connections, leaving
 * application code to provide URLs and extract results.
 * 
 * note:  
 *     Since, {@link org.springframework.web.servlet.DispatcherServlet} is
 *     used in this application it will support GET, HEAD, POST, PUT, PATCH and DELETE. 
 * 
 * @author Adam Abdulrehman
 * @version 1.0
 */
// @Secured("{ROLE_USER, ROLE_ADMIN}")
@Controller
@RequestMapping(value = "/airlines")
public final class AirlinesController {
	// private static final Logger logger = LoggerFactory.getLogger(AirlinesController.class);

	private final AirlinesResourceAssembler airlinesResourceAssembler;
	private final AirlineResourceAssembler airlineResourceAssembler;

	@Autowired
	private AirlinesService airlinesService;

	@Autowired
	AirlinesController(AirlinesResourceAssembler airlinesResourceAssembler,
			AirlineResourceAssembler airlineResourceAssembler) {
		this.airlinesResourceAssembler = airlinesResourceAssembler;
		this.airlineResourceAssembler = airlineResourceAssembler;
	}
	
	/**
	 * Retrieve all airlines for Arrivals
	 * 
	 * @return ResponseEntity containing AirlinesResource
	 * @throws AirlinesDoNotExistException to handle situation where airlines do not exist
	 */
	// @Secured("{ROLE_USER, ROLE_ADMIN}")
	@RequestMapping(method = RequestMethod.GET, value = "", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<AirlinesResource> getAirlines() throws AirlinesDoNotExistException {
		
		// retrieve all airlines through service layer from the Dao
		List <Airline> airlines = this.airlinesService.findAll();
		
		if(airlines.isEmpty()) {
			//Send Status code to say that there are no airlines to display
			throw new AirlinesDoNotExistException();
		}
		
		// convert the List of airline objects into List of airlineResource objects
		List<AirlineResource> resourceList = this.airlineResourceAssembler.toResources(airlines);
		
		// convert the List of airlineResource objects to an AirlinesResource
		AirlinesResource resource = this.airlinesResourceAssembler.toResource(resourceList);
		
		return new ResponseEntity<AirlinesResource>(resource, HttpStatus.OK);	
	}
	
	
	/**
	 * Retrieve an airline by airlineId
	 * 
	 * @param airlineId the Id of the airline being requested
	 * @return ResponseEntity containing AirlineResource
	 * @throws AirlineDoesNotExistException
	 * @throws MissingKeyException
	 */
	// @Secured("{ROLE_USER, ROLE_ADMIN}")
	@RequestMapping(method = RequestMethod.GET, value = "/{airlineId}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<AirlineResource> getAirlineById(@PathVariable long airlineId)
			throws AirlineDoesNotExistException, MissingKeyException {

		if(!this.airlinesService.findIfExistsById(airlineId)) {
			throw new MissingKeyException(airlineId);
		}
		Airline airline = this.airlinesService.findById(airlineId);
		
		if(airline == null) {
			throw new AirlineDoesNotExistException();
		}
		
		AirlineResource resource = this.airlineResourceAssembler.toResource(airline);
		return new ResponseEntity<AirlineResource>(resource, HttpStatus.OK);
	}
	
	/**
	 * create an airline with the given AirlineResource
	 * 
	 * @param airlineResource resource containing values for creating the airline
	 * @return ResponseEntity with a location header for the newly created resource
	 */
	// @Secured("{ROLE_ADMIN}")
	@RequestMapping(method = RequestMethod.POST, value = "")
	@ResponseBody
	public ResponseEntity<Void> createAirline(@RequestBody AirlineResource airlineResource) {
		
		String name = airlineResource.getAirlineName();
		Airline airline = new Airline(name);
		this.airlinesService.create(airline);
		
		// MediaTypes that are acceptable by this RequestMapping
		HttpHeaders headers = new HttpHeaders();
		List<MediaType> acceptableMediaTypes = new ArrayList<MediaType>();
		acceptableMediaTypes.add(MediaType.APPLICATION_JSON);
		headers.setAccept(acceptableMediaTypes);

		// set location to the newly created airline in Location header
		headers.setLocation(linkTo(AirlinesController.class).slash(airline).toUri());
		return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
	}
	
	/**
	 * Update an Airline
	 * 
	 * @param airlineId the Id of the airline being updated
	 * @param airlineResource resource containing values for updating the airline
	 * @return the http status
	 * @throws MissingKeyException
	 */
	// @Secured("{ROLE_USER, ROLE_ADMIN}")
	@RequestMapping(method = RequestMethod.PUT, value = "/{airlineId}")
	public ResponseEntity<String> updateAirline(@PathVariable long airlineId, @RequestBody AirlineResource airlineResource) throws MissingKeyException {
		
		String airlineName = airlineResource.getAirlineName();
		
		if(!this.airlinesService.findIfExistsById(airlineId)) {
			throw new MissingKeyException(airlineId);
		}
		Airline airline = new Airline(airlineId, airlineName);
		
		this.airlinesService.update(airline);
		return new ResponseEntity<String>(HttpStatus.OK);
	}
		
	/**
	 * Remove an Airline with given airlinesId
	 * 
	 * @param airlineId the Id of the airline being removed
	 * @return the http status
	 * @throws MissingKeyException
	 */
	// @Secured("{ROLE_ADMIN}")
	@RequestMapping(method = RequestMethod.DELETE, value = "/{airlineId}")
	public ResponseEntity<Void> removeAirline(@PathVariable long airlineId) throws MissingKeyException {
		
		if(!this.airlinesService.findIfExistsById(airlineId)) {
			throw new MissingKeyException(airlineId);
		}
		
		if(this.airlinesService.remove(airlineId)) {
			return new ResponseEntity<Void>(HttpStatus.OK);
		}
		return new ResponseEntity<Void>(HttpStatus.EXPECTATION_FAILED);
	}
}
