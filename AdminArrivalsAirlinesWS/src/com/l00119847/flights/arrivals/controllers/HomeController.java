package com.l00119847.flights.arrivals.controllers;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * This Controller is the main entry endpoint of the service. It sets the
 * resource location links. The HTTP request method used is HEAD since Location
 * headers are normally put there with a body being unnecessary.
 * 
 * @author Adam Abdulrehman
 * @version 1.0
 */
@Controller
@RequestMapping(value = "")
public final class HomeController {

	/**
	 * @return Response entity containing the location headers of resources
	 */
	@RequestMapping(method = RequestMethod.HEAD, value = "", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<Void> home() {
		HttpHeaders headers = new HttpHeaders();

		// set location to the concrete resources of airlines for arrivals
		headers.setLocation(linkTo(AirlinesController.class).toUri());
		return new ResponseEntity<Void>(headers, HttpStatus.OK);
	}
}