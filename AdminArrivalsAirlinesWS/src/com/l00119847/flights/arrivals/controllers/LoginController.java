package com.l00119847.flights.arrivals.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * This Controller handles login requests. It provides views for login, logout
 * and the access denied page.
 * 
 * @author Adam Abdulrehman
 * @version 1.0
 */
@Controller
public final class LoginController {

	@RequestMapping("/login")
	public String showLogin() {
		return "login";
	}

	@RequestMapping("/denied")
	public String showDenied() {
		return "denied";
	}

	@RequestMapping("/loggedout")
	public String showLoggedOut() {
		return "loggedout";
	}

}
