package com.l00119847.flights.arrivals.dao;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonProperty;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.hateoas.Identifiable;
import org.springframework.stereotype.Component;

/**
 * This Domain class represents an Airline. The annotated entity is bound to the
 * airlines table. Hibernate validation is used with annotations on the instance
 * variables. The constructor annotated with @JsonCreator provides a factory
 * method for instantiation.
 * 
 * @author Adam Abdulrehman
 * @version 1.0
 */
@Component
@Entity
@Table(name = "airlines")
public class Airline implements Identifiable<Long>, Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	private Long id;

	@NotBlank
	@Size(min = 2, max = 50)
	private String name;

	public Airline() {
	}

	@JsonCreator
	public Airline(@JsonProperty("name") String name) {
		this.name = name;
	}

	@JsonCreator
	public Airline(@JsonProperty("id") Long id,
			@JsonProperty("name") String name) {
		this.id = id;
		this.name = name;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}