package com.l00119847.flights.arrivals.dao;

import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.hateoas.ResourceSupport;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 * This class is the resource representation of an Airline. It contains
 * links to access the resource. It implements Serializable since that is
 * required for Json conversion.
 * 
 * This Wrapper class around Airline domain exposes only what is required.
 * The missing fields in comparison to the Airline domain class is deliberate 
 * so as to provide a more constrained representation. This is one of REST's
 * principle.
 * 
 * @author Adam Abdulrehman
 * @version 1.0
 */

@XmlRootElement(name = "airline")
@JsonIgnoreProperties(ignoreUnknown = true)
public class AirlineResource extends ResourceSupport {

	@JsonProperty("airlineName")
	private volatile String airlineName;

	public String getAirlineName() {
		return this.airlineName;
	}

	public void setAirlineName(String airlineName) {
		this.airlineName = airlineName;
	}
}
