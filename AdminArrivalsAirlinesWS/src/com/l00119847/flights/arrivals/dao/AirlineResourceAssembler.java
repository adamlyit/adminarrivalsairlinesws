package com.l00119847.flights.arrivals.dao;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;

import org.springframework.hateoas.ResourceAssembler;
import org.springframework.hateoas.ResourceSupport;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Component;

import com.l00119847.flights.arrivals.controllers.AirlinesController;

/**
 * This class handles the generation of the resource representation of an Airline. Given an
 * Airline it will return an AirlineResource. It also provides the Resource
 * object with links to access the resource. Links include: <br>
 * 1. self link to the resource <br>
 * 2. update link for the resource <br>
 * 3. delete link for the resource <br>
 * <p>
 * The class extends {@link ResourceAssemblerSupport} which is the base class to
 * implement {@link ResourceAssembler}s which will automate
 * {@link ResourceSupport} instance creation and make sure a self-link is always
 * added.
 * 
 * @author Adam Abdulrehman
 * @version 1.0
 */
@Component("airlineResourceAssembler")
public class AirlineResourceAssembler extends ResourceAssemblerSupport<Airline, AirlineResource> {

	public AirlineResourceAssembler() {
		super(AirlinesController.class, AirlineResource.class);
	}

	@Override
	public AirlineResource toResource(Airline airline) {
		AirlineResource resource = instantiateResource(airline);
		resource.setAirlineName(airline.getName());
		resource.add(linkTo(AirlinesController.class).slash(airline).withSelfRel());
		resource.add(linkTo(AirlinesController.class).slash(airline).withRel("update"));
		resource.add(linkTo(AirlinesController.class).slash(airline).withRel("delete"));

		return resource;
	}
}
