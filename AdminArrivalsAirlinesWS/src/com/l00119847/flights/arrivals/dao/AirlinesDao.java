package com.l00119847.flights.arrivals.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Component("airlinesDao")
@Transactional
public class AirlinesDao {

	@Autowired
	private SessionFactory sessionFactory;

	public Session session() {
		return sessionFactory.getCurrentSession();
	}

	public Airline findById(long airlineId) {

		Criteria criteria = session().createCriteria(Airline.class);
		criteria.add(Restrictions.idEq(airlineId));
		return (Airline) criteria.uniqueResult();
	}

	public Airline findByName(String airlineName) {

		Criteria criteria = session().createCriteria(Airline.class);
		criteria.add(Restrictions.eq("name", airlineName));
		return (Airline) criteria.uniqueResult();
	}

	
	public boolean findIfExistsByName(String airlineName) {
		return findByName(airlineName) != null;
	}
	
	public boolean findIfExistsById(long airlinesId) {
		return findById(airlinesId) != null;
	}
	
	@SuppressWarnings("unchecked")
	public List<Airline> findAll() {

		Criteria criteria = session().createCriteria(Airline.class);
		return criteria.list();
	}
	
	@SuppressWarnings("unchecked")
	public List<Airline> findAllByAirlineName(String name) {

		Criteria criteria = session().createCriteria(Airline.class);
		criteria.add(Restrictions.eq("name", name));
		return criteria.list();
	}
	
	@Transactional
	public boolean remove(Long airlineId) {
		Query query = session().createQuery("delete from Airline where id=:id");
		query.setLong("id", airlineId);

		return query.executeUpdate() == 1;
	}

	@Transactional
	public Airline create(Airline airline) {
		Long airlineId = (Long) session().save(airline);
		airline = findById(airlineId);
		return airline;
	}

	@Transactional
	public void update(Airline airline) {
		saveOrUpdate(airline);
	}
	
	@Transactional
	public void saveOrUpdate(Airline airline) {
		session().saveOrUpdate(airline);
	}

	// returns a new empty airline with an airlineId
	@Transactional
	public Airline create() {
		Airline airline = new Airline();
		Long airlineId = (Long) session().save(airline);
		airline = findById(airlineId);
		return airline;

	}

	@Transactional
	public boolean removeAll() {
		Query query = session().createQuery("delete from Airline");
		return query.executeUpdate() == 1;
	}
}