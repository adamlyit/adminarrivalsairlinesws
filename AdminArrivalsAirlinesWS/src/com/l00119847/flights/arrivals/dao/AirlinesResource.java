package com.l00119847.flights.arrivals.dao;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.springframework.hateoas.ResourceSupport;

@XmlRootElement(name = "airlines")
@JsonIgnoreProperties(ignoreUnknown = true)
public class AirlinesResource extends ResourceSupport {

	@JsonProperty("airlines")
	private volatile List<AirlineResource> airlines;

	public List<AirlineResource> getAirlines() {
		return airlines;
	}

	public void setAirlines(List<AirlineResource> airlines) {
		this.airlines = airlines;
	}
}
