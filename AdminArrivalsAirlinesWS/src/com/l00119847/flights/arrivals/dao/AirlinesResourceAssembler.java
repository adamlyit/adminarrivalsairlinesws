package com.l00119847.flights.arrivals.dao;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.ResourceAssembler;
import org.springframework.hateoas.ResourceSupport;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Component;

import com.l00119847.flights.arrivals.controllers.AirlinesController;

/**
 * 
 * This class handles the resource construction of an AirlinesResource. Given a
 * List of AirlineResources it will return an AirlinesResource. It also provides
 * the Resource object with links to access the resource. Links include: <br>
 * <li> self link to the resource with a rel of "airlines"
 * <li> create link for the resource with a rel of "create" <br><br>
 * <p>
 * The class extends {@link ResourceAssemblerSupport} which is the base class to
 * implement {@link ResourceAssembler}s which will automate
 * {@link ResourceSupport} instance creation.
 * 
 * @author Adam Abdulrehman
 * @version 1.0
 */

@Component("airlinesResourceAssembler")
public final class AirlinesResourceAssembler extends
		ResourceAssemblerSupport<List<AirlineResource>, AirlinesResource> {

	/**
	 * Bean to access the {@link AirlineResourceAssembler}
	 */
	@Autowired
	AirlineResourceAssembler airlineResourceAssembler;

	/**
	 * This constructor creates a new ResourceAssemblerSupport using the
	 * {@link AirlinesController} class and {@link AirlinesResource} type.
	 */
	public AirlinesResourceAssembler() {
		super(AirlinesController.class, AirlinesResource.class);
	}

	/**
	 * Converts the List of {@link AirlineResource} objects into an
	 * {@link AirlinesResource}.
	 */
	@Override
	public AirlinesResource toResource(List<AirlineResource> airlines) {

		AirlinesResource resource = instantiateResource(airlines);
		resource.setAirlines(airlines);
		resource.add(linkTo(AirlinesController.class).withRel("airlines"));
		resource.add(linkTo(AirlinesController.class).withRel("create"));
		return resource;
	}
}