package com.l00119847.flights.arrivals.exceptions;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * This Class is responsible for handling an exception when an airline does not exist
 * Exception Handled is one with HttpStatus 404 Not found
 * 
 * @author Adam Abdulrehman
 * @version 1.0
 */
@ResponseStatus(HttpStatus.NOT_FOUND)
public class AirlineDoesNotExistException extends Exception {

	private static final long serialVersionUID = 1L;

	public AirlineDoesNotExistException(String name) {
		super(String.format("Airline %s does not exist", name));
	}

	public AirlineDoesNotExistException() {
		super(String.format("Airline does not exist"));
	}

	public AirlineDoesNotExistException(long airlineId) {
		super(String.format("Airline with id %s does not exist", airlineId));
	}

}
