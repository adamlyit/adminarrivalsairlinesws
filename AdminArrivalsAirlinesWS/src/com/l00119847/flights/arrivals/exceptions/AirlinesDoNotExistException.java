package com.l00119847.flights.arrivals.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * This Class is responsible for handling an exception when no airlines exist
 * Exception Handled is one with HttpStatus 404 Not found
 * 
 * @author Adam Abdulrehman
 * @version 1.0
 */
@ResponseStatus(HttpStatus.NOT_FOUND)
public class AirlinesDoNotExistException extends Exception {

	private static final long serialVersionUID = 1L;

	
	/**
	 * Handling an exception when no airlines exist
	 * Exception Handled is one with HttpStatus 404 Not found
	 */
	public AirlinesDoNotExistException() {
		super("Airlines do not exist");
	}

}
