package com.l00119847.flights.arrivals.exceptions;

import java.nio.file.AccessDeniedException;

import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * This Class is responsible for handling Exceptions from the controller
 * 
 * @author Adam Abdulrehman
 * @version 1.0
 */
@ControllerAdvice
public final class ErrorHandler {

	/**
	 * @param ex DataAccessException which is a runtime exception caused by data access handled
	 * @return Response entity with a string indicating reason and an http status 500 Internal Server Error.
	 */
	@ExceptionHandler(DataAccessException.class)
	public ResponseEntity<String> handleDatabaseException(DataAccessException ex) {
		return new ResponseEntity<String>(String.format("{\"reason\": \"%s\"}", "Database error -" + ex.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	/**
	 * @param ex {@link AccessDeniedException} handled
	 * @return Response entity with a string indicating reason and an http status 401 Unauthorized.
	 */
	@ExceptionHandler(AccessDeniedException.class)
	public ResponseEntity<String> handleAccessException(AccessDeniedException ex) {
		return new ResponseEntity<String>(String.format("{\"reason\": \"%s\"}", ex.getMessage()), HttpStatus.UNAUTHORIZED);
	}
	
	/**
	 * @param ex {@link AirlineDoesNotExistException} and {@link AirlinesDoNotExistException} handled
	 * @return Response entity with a string indicating reason and an http status 404 Not Found.
	 */
	@ExceptionHandler({AirlineDoesNotExistException.class, AirlinesDoNotExistException.class})
	public ResponseEntity<String> handleNotFoundException(Exception ex) {
		return new ResponseEntity<String>(String.format("{\"reason\": \"%s\"}", ex.getMessage()), HttpStatus.NOT_FOUND);
	}
	
	/**
	 * @param ex {@link IllegalArgumentException} and {@link MissingKeyException} handled
	 * @return Response entity with a string indicating reason and an http status 400 Bad Request.
	 */
	@ExceptionHandler({IllegalArgumentException.class, MissingKeyException.class})
	public ResponseEntity<String> handleBadRequestException(Exception ex) {
		return new ResponseEntity<String>(String.format("{\"reason\": \"%s\"}", ex.getMessage()), HttpStatus.BAD_REQUEST);
	}
	
	/**
	 * @param ex {@link IllegalTransitionException} handled
	 * @return Response entity with a string indicating reason and an http status 409 Conflict.
	 */
	@ExceptionHandler(IllegalTransitionException.class)
	public ResponseEntity<String> handleConflictsException(Exception ex) {
		return new ResponseEntity<String>(String.format("{\"reason\": \"%s\"}", ex.getMessage()), HttpStatus.CONFLICT);
	}
}
