package com.l00119847.flights.arrivals.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * This Class is responsible for handling an exception where there is a
 * transition from one state to another that is not allowed. This exception is
 * in anticipation of a case where webflow is utilzed. For example, if there is
 * a flow from once an airline is created to where next a flight is to be
 * created then arrival.
 * 
 * Exception Handled is one with HttpStatus 409 Conflict
 * 
 * @author Adam Abdulrehman
 * @version 1.0
 */
@ResponseStatus(HttpStatus.CONFLICT)
public class IllegalTransitionException extends Exception {

	private static final long serialVersionUID = 1L;

	public IllegalTransitionException() {
		// can be specific here by passing which argument was illegal but this
		// suffices
		super(String.format("The request was illegal, a different transition is required"));
	}

}
