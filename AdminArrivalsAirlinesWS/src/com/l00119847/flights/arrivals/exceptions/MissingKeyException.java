package com.l00119847.flights.arrivals.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * This Class is responsible for handling an exception where a key or
 * id required is not found to be present.
 * 
 * Exception Handled is one with HttpStatus 400 Bad request
 * 
 * @author Adam Abdulrehman
 * @version 1.0
 */
@ResponseStatus(HttpStatus.BAD_REQUEST)
public class MissingKeyException extends Exception {

	private static final long serialVersionUID = 1L;

	public MissingKeyException(long id) {
		super(String.format("The Key " + id + " or Id provided is not found"));
	}
}
