package com.l00119847.flights.arrivals.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

/**
 * Authentication filter for REST services
 */
public class RestUsernamePasswordAuthenticationFilter extends
		UsernamePasswordAuthenticationFilter {
	@Override
	protected boolean requiresAuthentication(HttpServletRequest request,
			HttpServletResponse response) {
		
		boolean successfulAuthentication = false;
		String username = request.getParameter("j_username");
		String password = request.getParameter("j_password");
		
		if (username != null && password != null) {
			Authentication authResult = null;
			
			try {
				authResult = attemptAuthentication(request, response);
				if (authResult == null) {
					successfulAuthentication = false;
				}
			} 
			catch (AuthenticationException failed) {
				try {
					unsuccessfulAuthentication(request, response, failed);
				} catch (IOException e) {
					successfulAuthentication = false;
				} catch (ServletException e) {
					successfulAuthentication = false;
				}
				successfulAuthentication = false;
			}
			
			try {
				successfulAuthentication(request, response, null, authResult);
			} 
			catch (IOException e) {
				successfulAuthentication = false;
			} 
			catch (ServletException e) {
				successfulAuthentication = false;
			}
			return false;
		} else {
			successfulAuthentication = true;
		}
		return successfulAuthentication;
	}
}