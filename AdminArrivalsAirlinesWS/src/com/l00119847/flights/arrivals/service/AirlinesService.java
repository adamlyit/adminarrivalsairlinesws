package com.l00119847.flights.arrivals.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.l00119847.flights.arrivals.dao.Airline;
import com.l00119847.flights.arrivals.dao.AirlinesDao;

/**
 * This Service class handles access to the Dao. It provides a layer of abstraction
 * so that any implementation of Data acess is possible.
 * 
 * @author Adam Abdulrehman
 * @version 1.0
 */
@Service("airlinesService")
public class AirlinesService {

	@Autowired
	private AirlinesDao airlinesDao;

	/**
	 *
	 */
	public List<Airline> findAll() {
		return airlinesDao.findAll();
	}

	public Airline findById(long airlinesId) {
		return airlinesDao.findById(airlinesId);
	}
	
	public boolean findIfExistsById(long airlinesId) {
		return airlinesDao.findIfExistsById(airlinesId);
	}

	public Airline findByName(String airlineName) {
		return airlinesDao.findByName(airlineName);
	}
	
	public boolean findIfExistsByName(String airlineName) {
		return airlinesDao.findIfExistsByName(airlineName);
	}
	
	public void create(Airline airline) {
		airlinesDao.create(airline);
	}

	public void update(Airline airline) {
		airlinesDao.update(airline);
	}

	public boolean remove(long airlineId) {
		return airlinesDao.remove(airlineId);
	}
}
