package com.l00119847.flights.arrivals.tests.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import javax.sql.DataSource;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.l00119847.flights.arrivals.dao.Airline;
import com.l00119847.flights.arrivals.dao.AirlinesDao;

/**
 * This is a Test for the AirlinesDao
 * 
 * @author Adam Abdulrehman
 * @version 1.0
 */
@ActiveProfiles("dev")
@ContextConfiguration(locations = {
		"file:src/com/l00119847/flights/arrivals/config/dao-context.xml",
		"file:src/com/l00119847/flights/arrivals/tests/config/dataSource.xml" })
@RunWith(SpringJUnit4ClassRunner.class)
public class AirlinesDaoTest {

	@Autowired
	private DataSource dataSource;

	@Autowired
	private SessionFactory sessionFactory;

	@Autowired
	private AirlinesDao airlinesDao;

	Airline airlineUnderTest;
	String airlineName = "AdamsTestAirline";
	Long airlineId;

	public Session session() {
		return sessionFactory.getCurrentSession();
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		try {
			// create an airline for testing purposes
			this.airlinesDao.create(new Airline(airlineName));
			airlineUnderTest = this.airlinesDao.findByName(airlineName);
			airlineId = airlineUnderTest.getId();
			assertNotNull("Setup of airline failed", airlineUnderTest);
		} catch (Exception ex) {
			fail("Exception thrown by Db: " + ex.getMessage());
		}
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		try {
			boolean removed = this.airlinesDao.remove(airlineId);
			if (!removed) {
				fail("Failed to teardown the airline");
			}
			airlineUnderTest = this.airlinesDao.findByName(airlineName);
			assertNull("Teardown of airline failed", airlineUnderTest);
		} catch (Exception ex) {
			fail("Exception thrown by Db: " + ex.getMessage());
		}
	}

	/**
	 * Test method for
	 * {@link com.l00119847.flights.arrivals.tests.dao.AirlinesDao#findById(long)}
	 * .
	 */
	@Test
	public final void testFindById() {

		try {
			// get that Airline
			airlineUnderTest = this.airlinesDao.findById(airlineId);

			// Assert that it has been created
			assertNotNull("Airline does not exist", airlineUnderTest);
			assertEquals("Airline id mismatch", airlineUnderTest.getId(),
					airlineId);
		} catch (Exception ex) {
			fail("Exception thrown by Db: " + ex.getMessage());
		}
	}

	/**
	 * Test method for
	 * {@link com.l00119847.flights.arrivals.tests.dao.AirlinesDao#findByName(java.lang.String)}
	 * .
	 */
	@Test
	public final void testFindByName() {

		try {

			// get that Airline
			airlineUnderTest = this.airlinesDao.findByName(airlineName);
			airlineId = airlineUnderTest.getId();

			// Assert that it has been created
			assertNotNull("Airline does not exist", airlineUnderTest);
			assertEquals("Airline not found though it exists",
					airlineName.compareTo(airlineUnderTest.getName()), 0);
		} catch (Exception ex) {
			fail("Exception thrown by Db: " + ex.getMessage());
		}
	}

	/**
	 * Test method for checking if an airline exists by name
	 * 
	 * {@link com.l00119847.flights.arrivals.tests.dao.AirlinesDao#findIfExistsByName(java.lang.String)}
	 * .
	 */
	@Test
	public final void testFindIfExistsByName() {

		try {
			boolean found = this.airlinesDao.findIfExistsByName(airlineName);
			if (!found) {
				fail("Could not find an existing airline");
			}
		} catch (Exception ex) {
			fail("Exception thrown by Db: " + ex.getMessage());
		}
	}

	/**
	 * Test method for checking if an airline exists by Id
	 * 
	 * {@link com.l00119847.flights.arrivals.tests.dao.AirlinesDao#findIfExistsById(long)}
	 * .
	 */
	@Test
	public final void testFindIfExistsById() {

		try {

			boolean found = this.airlinesDao.findIfExistsById(airlineId);
			if (!found) {
				fail("Could not find an existing airline");
			}
		} catch (Exception ex) {
			fail("Exception thrown by Db: " + ex.getMessage());
		}
	}

	/**
	 * Test method for updating an Airline
	 * 
	 * {@link com.l00119847.flights.arrivals.tests.dao.AirlinesDao#update(com.l00119847.flights.arrivals.tests.dao.Airline)}
	 * .
	 */
	@Test
	public final void testUpdate() {

		String newAirlineName = "AdamsTestAirlineUpdate";

		try {
			// change the name of the airline
			airlineUnderTest.setName(newAirlineName);

			// update the airline name
			this.airlinesDao.update(airlineUnderTest);

			airlineUnderTest = this.airlinesDao.findByName(newAirlineName);
			assertTrue("The id of the update airline is incorrect",
					airlineId == airlineUnderTest.getId());
			assertEquals("The name of the update airline is incorrect",
					newAirlineName.compareTo(airlineUnderTest.getName()), 0);

			airlineUnderTest.setName(airlineName);
			this.airlinesDao.update(airlineUnderTest);
		} catch (Exception ex) {
			fail("Exception thrown by Db: " + ex.getMessage());
		}
	}
}
